﻿
namespace BibliotecaWebIces.Default {
    export interface TiposRow {
        Id?: number;
        Nombre?: string;
        Descripcion?: string;
    }

    export namespace TiposRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Default.Tipos';

        export namespace Fields {
            export declare const Id;
            export declare const Nombre;
            export declare const Descripcion;
        }

        [
            'Id',
            'Nombre',
            'Descripcion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}