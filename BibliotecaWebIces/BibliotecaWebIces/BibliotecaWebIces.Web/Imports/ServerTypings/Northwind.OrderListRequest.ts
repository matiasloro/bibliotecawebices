﻿namespace BibliotecaWebIces.Northwind {
    export interface OrderListRequest extends Serenity.ListRequest {
        ProductID?: number;
    }
}

