﻿
namespace BibliotecaWebIces.Default {
    export class TiposForm extends Serenity.PrefixedContext {
        static formKey = 'Default.Tipos';
    }

    export interface TiposForm {
        Nombre: Serenity.StringEditor;
        Descripcion: Serenity.StringEditor;
    }

    [,
        ['Nombre', () => Serenity.StringEditor],
        ['Descripcion', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(TiposForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}