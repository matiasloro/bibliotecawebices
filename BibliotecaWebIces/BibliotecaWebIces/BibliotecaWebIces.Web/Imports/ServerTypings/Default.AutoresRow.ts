﻿
namespace BibliotecaWebIces.Default {
    export interface AutoresRow {
        Id?: number;
        Nombre?: string;
        Apellido?: number;
        Edad?: number;
        FechaNacimiento?: string;
        Pais?: string;
    }

    export namespace AutoresRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Default.Autores';

        export namespace Fields {
            export declare const Id;
            export declare const Nombre;
            export declare const Apellido;
            export declare const Edad;
            export declare const FechaNacimiento;
            export declare const Pais;
        }

        [
            'Id',
            'Nombre',
            'Apellido',
            'Edad',
            'FechaNacimiento',
            'Pais'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}