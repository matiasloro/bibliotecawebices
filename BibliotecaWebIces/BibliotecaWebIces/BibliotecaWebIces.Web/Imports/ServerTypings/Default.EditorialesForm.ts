﻿
namespace BibliotecaWebIces.Default {
    export class EditorialesForm extends Serenity.PrefixedContext {
        static formKey = 'Default.Editoriales';
    }

    export interface EditorialesForm {
        Nombre: Serenity.StringEditor;
        Genero: Serenity.StringEditor;
        Pais: Serenity.StringEditor;
        Fundador: Serenity.StringEditor;
        Fundacion: Serenity.DateEditor;
    }

    [,
        ['Nombre', () => Serenity.StringEditor],
        ['Genero', () => Serenity.StringEditor],
        ['Pais', () => Serenity.StringEditor],
        ['Fundador', () => Serenity.StringEditor],
        ['Fundacion', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(EditorialesForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}