﻿
namespace BibliotecaWebIces.Default {
    export class CarrerasForm extends Serenity.PrefixedContext {
        static formKey = 'Default.Carreras';
    }

    export interface CarrerasForm {
        Nombre: Serenity.StringEditor;
        FechaAlta: Serenity.DateEditor;
        FechaBaja: Serenity.DateEditor;
        Descripcion: Serenity.StringEditor;
    }

    [,
        ['Nombre', () => Serenity.StringEditor],
        ['FechaAlta', () => Serenity.DateEditor],
        ['FechaBaja', () => Serenity.DateEditor],
        ['Descripcion', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(CarrerasForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}