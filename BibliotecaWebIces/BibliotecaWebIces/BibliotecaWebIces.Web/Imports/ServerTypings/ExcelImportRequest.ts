﻿namespace BibliotecaWebIces {
    export interface ExcelImportRequest extends Serenity.ServiceRequest {
        FileName?: string;
    }
}

