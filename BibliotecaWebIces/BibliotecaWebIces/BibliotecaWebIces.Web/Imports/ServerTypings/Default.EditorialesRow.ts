﻿
namespace BibliotecaWebIces.Default {
    export interface EditorialesRow {
        Id?: number;
        Nombre?: string;
        Genero?: string;
        Pais?: string;
        Fundador?: string;
        Fundacion?: string;
    }

    export namespace EditorialesRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Default.Editoriales';

        export namespace Fields {
            export declare const Id;
            export declare const Nombre;
            export declare const Genero;
            export declare const Pais;
            export declare const Fundador;
            export declare const Fundacion;
        }

        [
            'Id',
            'Nombre',
            'Genero',
            'Pais',
            'Fundador',
            'Fundacion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}