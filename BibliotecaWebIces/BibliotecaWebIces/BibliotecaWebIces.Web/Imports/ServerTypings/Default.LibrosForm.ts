﻿
namespace BibliotecaWebIces.Default {
    export class LibrosForm extends Serenity.PrefixedContext {
        static formKey = 'Default.Libros';
    }

    export interface LibrosForm {
        Titulo: Serenity.StringEditor;
        CantidadPaginas: Serenity.IntegerEditor;
        Isbn: Serenity.IntegerEditor;
        AñoPublicacion: Serenity.DateEditor;
        FechaIngreso: Serenity.DateEditor;
    }

    [,
        ['Titulo', () => Serenity.StringEditor],
        ['CantidadPaginas', () => Serenity.IntegerEditor],
        ['Isbn', () => Serenity.IntegerEditor],
        ['AñoPublicacion', () => Serenity.DateEditor],
        ['FechaIngreso', () => Serenity.DateEditor]
    ].forEach(x => Object.defineProperty(LibrosForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}