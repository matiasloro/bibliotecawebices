﻿namespace BibliotecaWebIces.Administration {
    export interface UserRoleListRequest extends Serenity.ServiceRequest {
        UserID?: number;
    }
}

