﻿
namespace BibliotecaWebIces.Default {
    export interface LibrosRow {
        Id?: number;
        Titulo?: string;
        CantidadPaginas?: number;
        Isbn?: number;
        AñoPublicacion?: string;
        FechaIngreso?: string;
    }

    export namespace LibrosRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Titulo';
        export const localTextPrefix = 'Default.Libros';

        export namespace Fields {
            export declare const Id;
            export declare const Titulo;
            export declare const CantidadPaginas;
            export declare const Isbn;
            export declare const AñoPublicacion;
            export declare const FechaIngreso;
        }

        [
            'Id',
            'Titulo',
            'CantidadPaginas',
            'Isbn',
            'AñoPublicacion',
            'FechaIngreso'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}