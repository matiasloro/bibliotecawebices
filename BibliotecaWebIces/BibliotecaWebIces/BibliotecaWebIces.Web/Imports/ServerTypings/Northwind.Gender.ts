﻿namespace BibliotecaWebIces.Northwind {
    export enum Gender {
        Male = 1,
        Female = 2
    }
    Serenity.Decorators.registerEnumType(Gender, 'BibliotecaWebIces.Northwind.Gender', 'BibliotecaWebIces.Northwind.Entities.Gender');
}

