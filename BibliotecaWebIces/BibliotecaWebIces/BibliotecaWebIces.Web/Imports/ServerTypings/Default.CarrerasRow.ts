﻿
namespace BibliotecaWebIces.Default {
    export interface CarrerasRow {
        Id?: number;
        Nombre?: string;
        FechaAlta?: string;
        FechaBaja?: string;
        Descripcion?: string;
    }

    export namespace CarrerasRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Nombre';
        export const localTextPrefix = 'Default.Carreras';

        export namespace Fields {
            export declare const Id;
            export declare const Nombre;
            export declare const FechaAlta;
            export declare const FechaBaja;
            export declare const Descripcion;
        }

        [
            'Id',
            'Nombre',
            'FechaAlta',
            'FechaBaja',
            'Descripcion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}