﻿
namespace BibliotecaWebIces.Default {
    export interface IdiomasRow {
        Id?: number;
        Idioma?: string;
        Descripcion?: string;
    }

    export namespace IdiomasRow {
        export const idProperty = 'Id';
        export const nameProperty = 'Idioma';
        export const localTextPrefix = 'Default.Idiomas';

        export namespace Fields {
            export declare const Id;
            export declare const Idioma;
            export declare const Descripcion;
        }

        [
            'Id',
            'Idioma',
            'Descripcion'
        ].forEach(x => (<any>Fields)[x] = x);
    }
}