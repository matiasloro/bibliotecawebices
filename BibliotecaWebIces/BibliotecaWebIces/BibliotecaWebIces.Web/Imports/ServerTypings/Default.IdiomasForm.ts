﻿
namespace BibliotecaWebIces.Default {
    export class IdiomasForm extends Serenity.PrefixedContext {
        static formKey = 'Default.Idiomas';
    }

    export interface IdiomasForm {
        Idioma: Serenity.StringEditor;
        Descripcion: Serenity.StringEditor;
    }

    [,
        ['Idioma', () => Serenity.StringEditor],
        ['Descripcion', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(IdiomasForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}