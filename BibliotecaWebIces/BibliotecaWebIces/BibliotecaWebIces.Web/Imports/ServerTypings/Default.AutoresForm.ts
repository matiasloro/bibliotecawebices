﻿
namespace BibliotecaWebIces.Default {
    export class AutoresForm extends Serenity.PrefixedContext {
        static formKey = 'Default.Autores';
    }

    export interface AutoresForm {
        Nombre: Serenity.StringEditor;
        Apellido: Serenity.IntegerEditor;
        Edad: Serenity.IntegerEditor;
        FechaNacimiento: Serenity.DateEditor;
        Pais: Serenity.StringEditor;
    }

    [,
        ['Nombre', () => Serenity.StringEditor],
        ['Apellido', () => Serenity.IntegerEditor],
        ['Edad', () => Serenity.IntegerEditor],
        ['FechaNacimiento', () => Serenity.DateEditor],
        ['Pais', () => Serenity.StringEditor]
    ].forEach(x => Object.defineProperty(AutoresForm.prototype, <string>x[0], {
        get: function () {
            return this.w(x[0], (x[1] as any)());
        },
        enumerable: true,
        configurable: true
    }));
}