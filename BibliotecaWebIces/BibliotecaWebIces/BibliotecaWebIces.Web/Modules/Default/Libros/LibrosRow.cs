﻿
namespace BibliotecaWebIces.Default.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Default"), TableName("[BWI].[Libros]")]
    [DisplayName("Libros"), InstanceName("Libro")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class LibrosRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Titulo"), Size(200), NotNull, QuickSearch]
        public String Titulo
        {
            get { return Fields.Titulo[this]; }
            set { Fields.Titulo[this] = value; }
        }

        [DisplayName("Cantidad Paginas")]
        public Int32? CantidadPaginas
        {
            get { return Fields.CantidadPaginas[this]; }
            set { Fields.CantidadPaginas[this] = value; }
        }

        [DisplayName("Isbn"), NotNull]
        public Int32? Isbn
        {
            get { return Fields.Isbn[this]; }
            set { Fields.Isbn[this] = value; }
        }

        [DisplayName("Año Publicacion")]
        public DateTime? AñoPublicacion
        {
            get { return Fields.AñoPublicacion[this]; }
            set { Fields.AñoPublicacion[this] = value; }
        }

        [DisplayName("Fecha Ingreso")]
        public DateTime? FechaIngreso
        {
            get { return Fields.FechaIngreso[this]; }
            set { Fields.FechaIngreso[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Titulo; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public LibrosRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Titulo;
            public Int32Field CantidadPaginas;
            public Int32Field Isbn;
            public DateTimeField AñoPublicacion;
            public DateTimeField FechaIngreso;
		}
    }
}
