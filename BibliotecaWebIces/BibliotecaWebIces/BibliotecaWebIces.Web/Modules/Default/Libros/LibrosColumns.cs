﻿
namespace BibliotecaWebIces.Default.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Default.Libros")]
    [BasedOnRow(typeof(Entities.LibrosRow), CheckNames = true)]
    public class LibrosColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        [EditLink]
        public String Titulo { get; set; }
        public Int32 CantidadPaginas { get; set; }
        public Int32 Isbn { get; set; }
        public DateTime AñoPublicacion { get; set; }
        public DateTime FechaIngreso { get; set; }
    }
}