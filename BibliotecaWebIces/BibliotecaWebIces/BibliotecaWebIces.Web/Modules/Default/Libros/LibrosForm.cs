﻿
namespace BibliotecaWebIces.Default.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Default.Libros")]
    [BasedOnRow(typeof(Entities.LibrosRow), CheckNames = true)]
    public class LibrosForm
    {
        public String Titulo { get; set; }
        public Int32 CantidadPaginas { get; set; }
        public Int32 Isbn { get; set; }
        public DateTime AñoPublicacion { get; set; }
        public DateTime FechaIngreso { get; set; }
    }
}