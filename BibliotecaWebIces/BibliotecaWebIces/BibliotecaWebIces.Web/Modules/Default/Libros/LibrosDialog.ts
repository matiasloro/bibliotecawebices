﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class LibrosDialog extends Serenity.EntityDialog<LibrosRow, any> {
        protected getFormKey() { return LibrosForm.formKey; }
        protected getIdProperty() { return LibrosRow.idProperty; }
        protected getLocalTextPrefix() { return LibrosRow.localTextPrefix; }
        protected getNameProperty() { return LibrosRow.nameProperty; }
        protected getService() { return LibrosService.baseUrl; }

        protected form = new LibrosForm(this.idPrefix);

    }
}