﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class LibrosGrid extends Serenity.EntityGrid<LibrosRow, any> {
        protected getColumnsKey() { return 'Default.Libros'; }
        protected getDialogType() { return LibrosDialog; }
        protected getIdProperty() { return LibrosRow.idProperty; }
        protected getLocalTextPrefix() { return LibrosRow.localTextPrefix; }
        protected getService() { return LibrosService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}