﻿
namespace BibliotecaWebIces.Default.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Default/Libros"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.LibrosRow))]
    public class LibrosController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Default/Libros/LibrosIndex.cshtml");
        }
    }
}