﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class EditorialesDialog extends Serenity.EntityDialog<EditorialesRow, any> {
        protected getFormKey() { return EditorialesForm.formKey; }
        protected getIdProperty() { return EditorialesRow.idProperty; }
        protected getLocalTextPrefix() { return EditorialesRow.localTextPrefix; }
        protected getNameProperty() { return EditorialesRow.nameProperty; }
        protected getService() { return EditorialesService.baseUrl; }

        protected form = new EditorialesForm(this.idPrefix);

    }
}