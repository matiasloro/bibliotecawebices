﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class EditorialesGrid extends Serenity.EntityGrid<EditorialesRow, any> {
        protected getColumnsKey() { return 'Default.Editoriales'; }
        protected getDialogType() { return EditorialesDialog; }
        protected getIdProperty() { return EditorialesRow.idProperty; }
        protected getLocalTextPrefix() { return EditorialesRow.localTextPrefix; }
        protected getService() { return EditorialesService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}