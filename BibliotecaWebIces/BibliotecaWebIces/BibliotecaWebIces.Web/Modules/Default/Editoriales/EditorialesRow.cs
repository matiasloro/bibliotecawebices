﻿
namespace BibliotecaWebIces.Default.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Default"), TableName("[BWI].[Editoriales]")]
    [DisplayName("Editoriales"), InstanceName("Editoriales")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class EditorialesRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Nombre"), Size(200), NotNull, QuickSearch]
        public String Nombre
        {
            get { return Fields.Nombre[this]; }
            set { Fields.Nombre[this] = value; }
        }

        [DisplayName("Genero"), Size(200), NotNull]
        public String Genero
        {
            get { return Fields.Genero[this]; }
            set { Fields.Genero[this] = value; }
        }

        [DisplayName("Pais"), Size(200), NotNull]
        public String Pais
        {
            get { return Fields.Pais[this]; }
            set { Fields.Pais[this] = value; }
        }

        [DisplayName("Fundador"), Size(200)]
        public String Fundador
        {
            get { return Fields.Fundador[this]; }
            set { Fields.Fundador[this] = value; }
        }

        [DisplayName("Fundacion")]
        public DateTime? Fundacion
        {
            get { return Fields.Fundacion[this]; }
            set { Fields.Fundacion[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Nombre; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public EditorialesRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Nombre;
            public StringField Genero;
            public StringField Pais;
            public StringField Fundador;
            public DateTimeField Fundacion;
		}
    }
}
