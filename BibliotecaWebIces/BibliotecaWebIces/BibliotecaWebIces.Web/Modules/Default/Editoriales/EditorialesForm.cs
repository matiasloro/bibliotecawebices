﻿
namespace BibliotecaWebIces.Default.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Default.Editoriales")]
    [BasedOnRow(typeof(Entities.EditorialesRow), CheckNames = true)]
    public class EditorialesForm
    {
        public String Nombre { get; set; }
        public String Genero { get; set; }
        public String Pais { get; set; }
        public String Fundador { get; set; }
        public DateTime Fundacion { get; set; }
    }
}