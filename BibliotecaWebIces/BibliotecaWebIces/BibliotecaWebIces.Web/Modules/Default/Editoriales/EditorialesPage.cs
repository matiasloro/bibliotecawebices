﻿
namespace BibliotecaWebIces.Default.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Default/Editoriales"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.EditorialesRow))]
    public class EditorialesController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Default/Editoriales/EditorialesIndex.cshtml");
        }
    }
}