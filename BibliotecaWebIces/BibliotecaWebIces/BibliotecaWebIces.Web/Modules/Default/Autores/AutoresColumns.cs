﻿
namespace BibliotecaWebIces.Default.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Default.Autores")]
    [BasedOnRow(typeof(Entities.AutoresRow), CheckNames = true)]
    public class AutoresColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        [EditLink]
        public String Nombre { get; set; }
        public Int32 Apellido { get; set; }
        public Int32 Edad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public String Pais { get; set; }
    }
}