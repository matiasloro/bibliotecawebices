﻿
namespace BibliotecaWebIces.Default.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Default"), TableName("[BWI].[Autores]")]
    [DisplayName("Autores"), InstanceName("Autores")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class AutoresRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Nombre"), Size(200), NotNull, QuickSearch]
        public String Nombre
        {
            get { return Fields.Nombre[this]; }
            set { Fields.Nombre[this] = value; }
        }

        [DisplayName("Apellido")]
        public Int32? Apellido
        {
            get { return Fields.Apellido[this]; }
            set { Fields.Apellido[this] = value; }
        }

        [DisplayName("Edad"), NotNull]
        public Int32? Edad
        {
            get { return Fields.Edad[this]; }
            set { Fields.Edad[this] = value; }
        }

        [DisplayName("Fecha Nacimiento")]
        public DateTime? FechaNacimiento
        {
            get { return Fields.FechaNacimiento[this]; }
            set { Fields.FechaNacimiento[this] = value; }
        }

        [DisplayName("Pais"), Size(255)]
        public String Pais
        {
            get { return Fields.Pais[this]; }
            set { Fields.Pais[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Nombre; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public AutoresRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Nombre;
            public Int32Field Apellido;
            public Int32Field Edad;
            public DateTimeField FechaNacimiento;
            public StringField Pais;
		}
    }
}
