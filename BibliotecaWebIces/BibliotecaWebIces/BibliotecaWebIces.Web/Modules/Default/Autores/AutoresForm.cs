﻿
namespace BibliotecaWebIces.Default.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Default.Autores")]
    [BasedOnRow(typeof(Entities.AutoresRow), CheckNames = true)]
    public class AutoresForm
    {
        public String Nombre { get; set; }
        public Int32 Apellido { get; set; }
        public Int32 Edad { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public String Pais { get; set; }
    }
}