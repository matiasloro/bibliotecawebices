﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class AutoresDialog extends Serenity.EntityDialog<AutoresRow, any> {
        protected getFormKey() { return AutoresForm.formKey; }
        protected getIdProperty() { return AutoresRow.idProperty; }
        protected getLocalTextPrefix() { return AutoresRow.localTextPrefix; }
        protected getNameProperty() { return AutoresRow.nameProperty; }
        protected getService() { return AutoresService.baseUrl; }

        protected form = new AutoresForm(this.idPrefix);

    }
}