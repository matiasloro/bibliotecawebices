﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class AutoresGrid extends Serenity.EntityGrid<AutoresRow, any> {
        protected getColumnsKey() { return 'Default.Autores'; }
        protected getDialogType() { return AutoresDialog; }
        protected getIdProperty() { return AutoresRow.idProperty; }
        protected getLocalTextPrefix() { return AutoresRow.localTextPrefix; }
        protected getService() { return AutoresService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}