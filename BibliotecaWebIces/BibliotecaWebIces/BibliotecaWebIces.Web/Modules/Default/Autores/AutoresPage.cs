﻿
namespace BibliotecaWebIces.Default.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Default/Autores"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.AutoresRow))]
    public class AutoresController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Default/Autores/AutoresIndex.cshtml");
        }
    }
}