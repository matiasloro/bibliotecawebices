﻿
namespace BibliotecaWebIces.Default.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Default/Carreras"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.CarrerasRow))]
    public class CarrerasController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Default/Carreras/CarrerasIndex.cshtml");
        }
    }
}