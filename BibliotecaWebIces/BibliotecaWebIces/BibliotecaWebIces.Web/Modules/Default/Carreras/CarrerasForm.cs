﻿
namespace BibliotecaWebIces.Default.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Default.Carreras")]
    [BasedOnRow(typeof(Entities.CarrerasRow), CheckNames = true)]
    public class CarrerasForm
    {
        public String Nombre { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime FechaBaja { get; set; }
        public String Descripcion { get; set; }
    }
}