﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class CarrerasGrid extends Serenity.EntityGrid<CarrerasRow, any> {
        protected getColumnsKey() { return 'Default.Carreras'; }
        protected getDialogType() { return CarrerasDialog; }
        protected getIdProperty() { return CarrerasRow.idProperty; }
        protected getLocalTextPrefix() { return CarrerasRow.localTextPrefix; }
        protected getService() { return CarrerasService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}