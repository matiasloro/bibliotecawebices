﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class CarrerasDialog extends Serenity.EntityDialog<CarrerasRow, any> {
        protected getFormKey() { return CarrerasForm.formKey; }
        protected getIdProperty() { return CarrerasRow.idProperty; }
        protected getLocalTextPrefix() { return CarrerasRow.localTextPrefix; }
        protected getNameProperty() { return CarrerasRow.nameProperty; }
        protected getService() { return CarrerasService.baseUrl; }

        protected form = new CarrerasForm(this.idPrefix);

    }
}