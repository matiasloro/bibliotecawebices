﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class IdiomasGrid extends Serenity.EntityGrid<IdiomasRow, any> {
        protected getColumnsKey() { return 'Default.Idiomas'; }
        protected getDialogType() { return IdiomasDialog; }
        protected getIdProperty() { return IdiomasRow.idProperty; }
        protected getLocalTextPrefix() { return IdiomasRow.localTextPrefix; }
        protected getService() { return IdiomasService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}