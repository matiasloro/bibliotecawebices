﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class IdiomasDialog extends Serenity.EntityDialog<IdiomasRow, any> {
        protected getFormKey() { return IdiomasForm.formKey; }
        protected getIdProperty() { return IdiomasRow.idProperty; }
        protected getLocalTextPrefix() { return IdiomasRow.localTextPrefix; }
        protected getNameProperty() { return IdiomasRow.nameProperty; }
        protected getService() { return IdiomasService.baseUrl; }

        protected form = new IdiomasForm(this.idPrefix);

    }
}