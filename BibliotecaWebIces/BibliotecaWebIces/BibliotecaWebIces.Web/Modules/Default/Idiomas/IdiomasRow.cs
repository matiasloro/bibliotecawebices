﻿
namespace BibliotecaWebIces.Default.Entities
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using Serenity.Data.Mapping;
    using System;
    using System.ComponentModel;
    using System.IO;

    [ConnectionKey("Default"), Module("Default"), TableName("[BWI].[Idiomas]")]
    [DisplayName("Idiomas"), InstanceName("Idiomas")]
    [ReadPermission("Administration:General")]
    [ModifyPermission("Administration:General")]
    public sealed class IdiomasRow : Row, IIdRow, INameRow
    {
        [DisplayName("Id"), Identity]
        public Int32? Id
        {
            get { return Fields.Id[this]; }
            set { Fields.Id[this] = value; }
        }

        [DisplayName("Idioma"), Size(60), NotNull, QuickSearch]
        public String Idioma
        {
            get { return Fields.Idioma[this]; }
            set { Fields.Idioma[this] = value; }
        }

        [DisplayName("Descripcion"), Size(200)]
        public String Descripcion
        {
            get { return Fields.Descripcion[this]; }
            set { Fields.Descripcion[this] = value; }
        }

        IIdField IIdRow.IdField
        {
            get { return Fields.Id; }
        }

        StringField INameRow.NameField
        {
            get { return Fields.Idioma; }
        }

        public static readonly RowFields Fields = new RowFields().Init();

        public IdiomasRow()
            : base(Fields)
        {
        }

        public class RowFields : RowFieldsBase
        {
            public Int32Field Id;
            public StringField Idioma;
            public StringField Descripcion;
		}
    }
}
