﻿
namespace BibliotecaWebIces.Default.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Default/Idiomas"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.IdiomasRow))]
    public class IdiomasController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Default/Idiomas/IdiomasIndex.cshtml");
        }
    }
}