﻿
namespace BibliotecaWebIces.Default.Forms
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [FormScript("Default.Tipos")]
    [BasedOnRow(typeof(Entities.TiposRow), CheckNames = true)]
    public class TiposForm
    {
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
    }
}