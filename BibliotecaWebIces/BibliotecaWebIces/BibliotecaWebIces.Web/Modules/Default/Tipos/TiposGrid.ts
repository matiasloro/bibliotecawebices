﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class TiposGrid extends Serenity.EntityGrid<TiposRow, any> {
        protected getColumnsKey() { return 'Default.Tipos'; }
        protected getDialogType() { return TiposDialog; }
        protected getIdProperty() { return TiposRow.idProperty; }
        protected getLocalTextPrefix() { return TiposRow.localTextPrefix; }
        protected getService() { return TiposService.baseUrl; }

        constructor(container: JQuery) {
            super(container);
        }
    }
}