﻿
namespace BibliotecaWebIces.Default.Pages
{
    using Serenity;
    using Serenity.Web;
    using System.Web.Mvc;

    [RoutePrefix("Default/Tipos"), Route("{action=index}")]
    [PageAuthorize(typeof(Entities.TiposRow))]
    public class TiposController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Modules/Default/Tipos/TiposIndex.cshtml");
        }
    }
}