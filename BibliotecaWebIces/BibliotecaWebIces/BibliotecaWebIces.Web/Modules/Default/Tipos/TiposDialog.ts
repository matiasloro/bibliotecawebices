﻿
namespace BibliotecaWebIces.Default {

    @Serenity.Decorators.registerClass()
    export class TiposDialog extends Serenity.EntityDialog<TiposRow, any> {
        protected getFormKey() { return TiposForm.formKey; }
        protected getIdProperty() { return TiposRow.idProperty; }
        protected getLocalTextPrefix() { return TiposRow.localTextPrefix; }
        protected getNameProperty() { return TiposRow.nameProperty; }
        protected getService() { return TiposService.baseUrl; }

        protected form = new TiposForm(this.idPrefix);

    }
}