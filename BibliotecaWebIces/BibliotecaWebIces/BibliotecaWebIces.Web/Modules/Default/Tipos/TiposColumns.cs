﻿
namespace BibliotecaWebIces.Default.Columns
{
    using Serenity;
    using Serenity.ComponentModel;
    using Serenity.Data;
    using System;
    using System.ComponentModel;
    using System.Collections.Generic;
    using System.IO;

    [ColumnsScript("Default.Tipos")]
    [BasedOnRow(typeof(Entities.TiposRow), CheckNames = true)]
    public class TiposColumns
    {
        [EditLink, DisplayName("Db.Shared.RecordId"), AlignRight]
        public Int32 Id { get; set; }
        [EditLink]
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
    }
}