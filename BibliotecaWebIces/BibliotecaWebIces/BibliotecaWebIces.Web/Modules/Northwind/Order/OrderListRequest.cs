﻿using Serenity.Services;

namespace BibliotecaWebIces.Northwind
{
    public class OrderListRequest : ListRequest
    {
        public int? ProductID { get; set; }
    }
}