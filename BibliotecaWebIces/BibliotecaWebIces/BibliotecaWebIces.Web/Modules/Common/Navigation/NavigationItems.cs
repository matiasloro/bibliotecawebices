﻿using Serenity.Navigation;
using Administration = BibliotecaWebIces.Administration.Pages;
using MyPages = BibliotecaWebIces.Default.Pages;

//[assembly: NavigationLink(1000, "Dashboard", url: "~/", permission: "", icon: "fa-tachometer")]

[assembly: NavigationLink(2100, "Libros", typeof(MyPages.LibrosController), icon: "fa fa-book")]
[assembly: NavigationLink(2100, "Autores", typeof(MyPages.AutoresController), icon: "fa fa-user-circle")]
[assembly: NavigationLink(2100, "Carreras", typeof(MyPages.CarrerasController), icon: "fa-folder-o")]
[assembly: NavigationLink(2100, "Editoriales", typeof(MyPages.EditorialesController), icon: "fa-files-o")]
[assembly: NavigationLink(2100, "Idiomas", typeof(MyPages.IdiomasController), icon: "fa-comments")]
[assembly: NavigationLink(2100, "Tipos", typeof(MyPages.TiposController), icon: "fa fa-list")]
