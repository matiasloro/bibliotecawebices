﻿
namespace BibliotecaWebIces.Membership
{
    public class ResetPasswordModel
    {
        public string Token { get; set; }
    }
}