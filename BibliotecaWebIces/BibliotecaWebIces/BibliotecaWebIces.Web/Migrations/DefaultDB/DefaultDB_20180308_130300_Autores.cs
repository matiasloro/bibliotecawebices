﻿using FluentMigrator;

namespace BibliotecaWebIces.Migrations.DefaultDB
{
    [Migration(20180308130300)]
    public class DefaultDB_20180308_130300_Autores : Migration
    {
        public override void Up()
        {
            Create.Table("Autores").InSchema("BWI")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Nombre").AsString(200).NotNullable()
                .WithColumn("Apellido").AsInt32().Nullable()
                .WithColumn("Edad").AsInt32().NotNullable()
                .WithColumn("FechaNacimiento").AsDateTime().Nullable()
                .WithColumn("Pais").AsString().Nullable();
                
        }

        public override void Down()
        {

        }
    }

}