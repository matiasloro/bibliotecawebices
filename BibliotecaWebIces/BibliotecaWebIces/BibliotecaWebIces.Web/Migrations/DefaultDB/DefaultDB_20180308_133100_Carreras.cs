﻿using FluentMigrator;

namespace BibliotecaWebIces.Migrations.DefaultDB
{
    [Migration(20180308133100)]
    public class DefaultDB_20180308_133100_Carreras : Migration
    {
        public override void Up()
        {
            Create.Table("Carreras").InSchema("BWI")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Nombre").AsString(200).NotNullable()
                .WithColumn("FechaAlta").AsDateTime().Nullable()
                .WithColumn("FechaBaja").AsDateTime().Nullable()
                .WithColumn("Descripcion").AsString(200).Nullable();
                
        }

        public override void Down()
        {

        }
    }

}