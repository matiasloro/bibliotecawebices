﻿using FluentMigrator;

namespace BibliotecaWebIces.Migrations.DefaultDB
{
    [Migration(20180308103600)]
    public class DefaultDB_20180308_103600_Libros : Migration
    {
        public override void Up()
        {
            Create.Schema("BWI");

            Create.Table("Libros").InSchema("BWI")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Titulo").AsString(200).NotNullable()
                .WithColumn("CantidadPaginas").AsInt32().Nullable()
                .WithColumn("Isbn").AsInt32().NotNullable()
                .WithColumn("AñoPublicacion").AsDateTime().Nullable()
                .WithColumn("FechaIngreso").AsDateTime().Nullable();
                
        }

        public override void Down()
        {

        }
    }

}