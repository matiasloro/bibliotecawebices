﻿using FluentMigrator;

namespace BibliotecaWebIces.Migrations.DefaultDB
{
    [Migration(20180308134600)]
    public class DefaultDB_20180308_134600_Tipos : Migration
    {
        public override void Up()
        {
            

            Create.Table("Tipos").InSchema("BWI")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Nombre").AsString(60).NotNullable()
                .WithColumn("Descripcion").AsString(200).Nullable();
                                
        }

        public override void Down()
        {

        }
    }

}