﻿using FluentMigrator;

namespace BibliotecaWebIces.Migrations.DefaultDB
{
    [Migration(20180308133600)]
    public class DefaultDB_20180308_133600_Editoriales : Migration
    {
        public override void Up()
        {
                Create.Table("Editoriales").InSchema("BWI")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Nombre").AsString(200).NotNullable()
                .WithColumn("Genero").AsString(200).NotNullable()
                .WithColumn("Pais").AsString(200).NotNullable()
                .WithColumn("Fundador").AsString(200).Nullable()
                .WithColumn("Fundacion").AsDateTime().Nullable();
                
        }

        public override void Down()
        {

        }
    }

}